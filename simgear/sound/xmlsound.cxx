// sound.cxx -- Sound class implementation
//
// Started by Erik Hofman, February 2002
// (Reuses some code from  fg_fx.cxx created by David Megginson)
//
// Copyright (C) 2002  Curtis L. Olson - http://www.flightgear.org/~curt
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifdef HAVE_CONFIG_H
#  include <simgear_config.h>
#endif

#include <simgear/compiler.h>

#include <string.h>

#include <simgear/debug/logstream.hxx>
#include <simgear/props/condition.hxx>
#include <simgear/math/SGMath.hxx>


#include "xmlsound.hxx"


// static double _snd_lin(double v)   { return v; }
static double _snd_inv(double v)   { return (v == 0) ? 1e99 : 1/v; }
static double _snd_abs(double v)   { return (v >= 0) ? v : -v; }
static double _snd_sqrt(double v)  { return sqrt(fabs(v)); }
static double _snd_log10(double v) { return log10(fabs(v)); }
static double _snd_log(double v)   { return log(fabs(v)); }
// static double _snd_sqr(double v)   { return v*v; }
// static double _snd_pow3(double v)  { return v*v*v; }

static const struct {
	const char *name;
	double (*fn)(double);
} __sound_fn[] = {
//	{"lin", _snd_lin},
	{"inv", _snd_inv},
	{"abs", _snd_abs},
	{"sqrt", _snd_sqrt},
	{"log", _snd_log10},
	{"ln", _snd_log},
//	{"sqr", _snd_sqr},
//	{"pow3", _snd_pow3},
	{"", NULL}
};

SGXmlSound::SGXmlSound()
  : _sample(NULL),
    _condition(NULL),
    _active(false),
    _name(""),
    _mode(SGXmlSound::ONCE),
    _prev_value(0),
    _dt_play(0.0),
    _dt_stop(0.0),
    _delay(0.0),
    _stopping(0.0)
{
}

SGXmlSound::~SGXmlSound()
{
    if (_sample)
        _sample->stop();

    delete _condition;

    _volume.clear();
    _pitch.clear();
}

void
SGXmlSound::init(SGPropertyNode *root, SGPropertyNode *node, SGSoundMgr *sndmgr,
                 const string &path)
{

   //
   // set global sound properties
   //
   
   _name = node->getStringValue("name", "");
   SG_LOG(SG_GENERAL, SG_INFO, "Loading sound information for: " << _name );

   const char *mode_str = node->getStringValue("mode", "");
   if ( !strcmp(mode_str, "looped") ) {
       _mode = SGXmlSound::LOOPED;

   } else if ( !strcmp(mode_str, "in-transit") ) {
       _mode = SGXmlSound::IN_TRANSIT;

   } else {
      _mode = SGXmlSound::ONCE;

      if ( strcmp(mode_str, "") )
         SG_LOG(SG_GENERAL,SG_INFO, "  Unknown sound mode, default to 'once'");
   }

   _property = root->getNode(node->getStringValue("property", ""), true);
   SGPropertyNode *condition = node->getChild("condition");
   if (condition != NULL)
      _condition = sgReadCondition(root, condition);

   if (!_property && !_condition)
      SG_LOG(SG_GENERAL, SG_WARN,
             "  Neither a condition nor a property specified");

   _delay = node->getDoubleValue("delay-sec", 0.0);

   //
   // set volume properties
   //
   unsigned int i;
   float v = 0.0;
   std::vector<SGPropertyNode_ptr> kids = node->getChildren("volume");
   for (i = 0; (i < kids.size()) && (i < SGXmlSound::MAXPROP); i++) {
      _snd_prop volume = {NULL, NULL, NULL, 1.0, 0.0, 0.0, 0.0, false};

      if (strcmp(kids[i]->getStringValue("property"), ""))
         volume.prop = root->getNode(kids[i]->getStringValue("property", ""), true);

      const char *intern_str = kids[i]->getStringValue("internal", "");
      if (!strcmp(intern_str, "dt_play"))
         volume.intern = &_dt_play;
      else if (!strcmp(intern_str, "dt_stop"))
         volume.intern = &_dt_stop;

      if ((volume.factor = kids[i]->getDoubleValue("factor", 1.0)) != 0.0)
         if (volume.factor < 0.0) {
            volume.factor = -volume.factor;
            volume.subtract = true;
         }

      const char *type_str = kids[i]->getStringValue("type", "");
      if ( strcmp(type_str, "") ) {

         for (int j=0; __sound_fn[j].fn; j++)
           if ( !strcmp(type_str, __sound_fn[j].name) ) {
               volume.fn = __sound_fn[j].fn;
               break;
            }

         if (!volume.fn)
            SG_LOG(SG_GENERAL,SG_INFO,
                   "  Unknown volume type, default to 'lin'");
      }

      volume.offset = kids[i]->getDoubleValue("offset", 0.0);

      if ((volume.min = kids[i]->getDoubleValue("min", 0.0)) < 0.0)
         SG_LOG( SG_GENERAL, SG_WARN,
          "Volume minimum value below 0. Forced to 0.");

      volume.max = kids[i]->getDoubleValue("max", 0.0);
      if (volume.max && (volume.max < volume.min) )
         SG_LOG(SG_GENERAL,SG_ALERT,
                "  Volume maximum below minimum. Neglected.");

      _volume.push_back(volume);
      v += volume.offset;

   }

   float reference_dist = node->getDoubleValue("reference-dist", 500.0);
   float max_dist = node->getDoubleValue("max-dist", 3000.0);
 
   //
   // set pitch properties
   //
   float p = 0.0;
   kids = node->getChildren("pitch");
   for (i = 0; (i < kids.size()) && (i < SGXmlSound::MAXPROP); i++) {
      _snd_prop pitch = {NULL, NULL, NULL, 1.0, 1.0, 0.0, 0.0, false};

      if (strcmp(kids[i]->getStringValue("property", ""), ""))
         pitch.prop = root->getNode(kids[i]->getStringValue("property", ""), true);

      const char *intern_str = kids[i]->getStringValue("internal", "");
      if (!strcmp(intern_str, "dt_play"))
         pitch.intern = &_dt_play;
      else if (!strcmp(intern_str, "dt_stop"))
         pitch.intern = &_dt_stop;

      if ((pitch.factor = kids[i]->getDoubleValue("factor", 1.0)) != 0.0)
         if (pitch.factor < 0.0) {
            pitch.factor = -pitch.factor;
            pitch.subtract = true;
         }

      const char *type_str = kids[i]->getStringValue("type", "");
      if ( strcmp(type_str, "") ) {

         for (int j=0; __sound_fn[j].fn; j++) 
            if ( !strcmp(type_str, __sound_fn[j].name) ) {
               pitch.fn = __sound_fn[j].fn;
               break;
            }

         if (!pitch.fn)
            SG_LOG(SG_GENERAL,SG_INFO,
                   "  Unknown pitch type, default to 'lin'");
      }
     
      pitch.offset = kids[i]->getDoubleValue("offset", 1.0);

      if ((pitch.min = kids[i]->getDoubleValue("min", 0.0)) < 0.0)
         SG_LOG(SG_GENERAL,SG_WARN,
                "  Pitch minimum value below 0. Forced to 0.");

      pitch.max = kids[i]->getDoubleValue("max", 0.0);
      if (pitch.max && (pitch.max < pitch.min) )
         SG_LOG(SG_GENERAL,SG_ALERT,
                "  Pitch maximum below minimum. Neglected");

      _pitch.push_back(pitch);
      p += pitch.offset;
   }

   //
   // Relative position
   //
   sgVec3 offset_pos;
   sgSetVec3( offset_pos, 0.0, 0.0, 0.0 );
   SGPropertyNode_ptr pos = node->getChild("position");
   if ( pos != NULL ) {
       offset_pos[0] = pos->getDoubleValue("x", 0.0);
       offset_pos[1] = -pos->getDoubleValue("y", 0.0);
       offset_pos[2] = pos->getDoubleValue("z", 0.0);
   }

   //
   // Orientation
   //
   sgVec3 dir;
   float inner, outer, outer_gain;
   sgSetVec3( dir, 0.0, 0.0, 0.0 );
   inner = outer = 360.0;
   outer_gain = 0.0;
   pos = node->getChild("orientation");
   if ( pos != NULL ) {
      dir[0] = pos->getDoubleValue("x", 0.0);
      dir[1] = -pos->getDoubleValue("y", 0.0);
      dir[2] = pos->getDoubleValue("z", 0.0);
      inner = pos->getDoubleValue("inner-angle", 360.0);
      outer = pos->getDoubleValue("outer-angle", 360.0);
      outer_gain = pos->getDoubleValue("outer-gain", 0.0);
   }
   
   //
   // Initialize the sample
   //
   _mgr = sndmgr;
   if ( (_sample = _mgr->find(_name)) == NULL ) {
       // FIXME: Does it make sense to overwrite a previous entry's
       // configuration just because a new entry has the same name?
       // Note that we can't match on identical "path" because we the
       // new entry could be at a different location with different
       // configuration so we need a new sample which creates a new
       // "alSource".  The semantics of what is going on here seems
       // confused and needs to be thought through more carefully.
        _sample = new SGSoundSample( path.c_str(),
                                    node->getStringValue("path", ""),
                                    false );

       _mgr->add( _sample, _name );
   }

   _sample->set_offset_pos( offset_pos );
   _sample->set_orientation(dir, inner, outer, outer_gain);
   _sample->set_volume(v);
   _sample->set_reference_dist( reference_dist );
   _sample->set_max_dist( max_dist );
   _sample->set_pitch(p);
}

void
SGXmlSound::update (double dt)
{
   double curr_value = 0.0;

   //
   // If the state changes to false, stop playing.
   //
   if (_property)
       curr_value = _property->getDoubleValue();

   if (							// Lisp, anyone?
       (_condition && !_condition->test()) ||
       (!_condition && _property &&
        (
         !curr_value ||
         ( (_mode == SGXmlSound::IN_TRANSIT) && (curr_value == _prev_value) )
         )
        )
       )
   {
       if ((_mode != SGXmlSound::IN_TRANSIT) || (_stopping > MAX_TRANSIT_TIME)) {
           if (_sample->is_playing()) {
               SG_LOG(SG_GENERAL, SG_INFO, "Stopping audio after " << _dt_play
                      << " sec: " << _name );

               _sample->stop();
           }

           _active = false;
           _dt_stop += dt;
           _dt_play = 0.0;
       } else {
           _stopping += dt;
       }

       return;
   }

   //
   // mode is ONCE and the sound is still playing?
   //
   if (_active && (_mode == SGXmlSound::ONCE)) {

      if (!_sample->is_playing()) {
         _dt_stop += dt;
         _dt_play = 0.0;
      } else {
         _dt_play += dt;
      }

   } else {

      //
      // Update the playing time, cache the current value and
      // clear the delay timer.
      //
      _dt_play += dt;
      _prev_value = curr_value;
      _stopping = 0.0;
   }

   if (_dt_play < _delay)
      return;

   //
   // Update the volume
   //
   int i;
   int max = _volume.size();
   double volume = 1.0;
   double volume_offset = 0.0;

   for(i = 0; i < max; i++) {
      double v = 1.0;

      if (_volume[i].prop)
         v = _volume[i].prop->getDoubleValue();

      else if (_volume[i].intern)
         v = *_volume[i].intern;

      if (_volume[i].fn)
         v = _volume[i].fn(v);

      v *= _volume[i].factor;

      if (_volume[i].max && (v > _volume[i].max))
         v = _volume[i].max;

      else if (v < _volume[i].min)
         v = _volume[i].min;

      if (_volume[i].subtract)				// Hack!
         volume = _volume[i].offset - v;

      else {
         volume_offset += _volume[i].offset;
         volume *= v;
      }
   }

   //
   // Update the pitch
   //
   max = _pitch.size();
   double pitch = 1.0;
   double pitch_offset = 0.0;

   for(i = 0; i < max; i++) {
      double p = 1.0;

      if (_pitch[i].prop)
         p = _pitch[i].prop->getDoubleValue();

      else if (_pitch[i].intern)
         p = *_pitch[i].intern;

      if (_pitch[i].fn)
         p = _pitch[i].fn(p);

      p *= _pitch[i].factor;

      if (_pitch[i].max && (p > _pitch[i].max))
         p = _pitch[i].max;

      else if (p < _pitch[i].min)
         p = _pitch[i].min;

      if (_pitch[i].subtract)				// Hack!
         pitch = _pitch[i].offset - p;

      else {
         pitch_offset += _pitch[i].offset;
         pitch *= p;
      }
   }

   //
   // Change sample state
   //

   double vol = volume_offset + volume;
   if (vol > 1.0) {
      SG_LOG(SG_GENERAL, SG_WARN, "Sound volume too large for '"
              << _name << "':  " << vol << "  ->  clipping to 1.0");
      vol = 1.0;
   }
   _sample->set_volume(vol);
   _sample->set_pitch( pitch_offset + pitch );


   //
   // Do we need to start playing the sample?
   //
   if (!_active) {

      if (_mode == SGXmlSound::ONCE)
         _sample->play(false);

      else
         _sample->play(true);

      SG_LOG(SG_GENERAL, SG_INFO, "Playing audio after " << _dt_stop 
                                   << " sec: " << _name);
      SG_LOG(SG_GENERAL, SG_BULK,
                         "Playing " << ((_mode == ONCE) ? "once" : "looped"));

      _active = true;
      _dt_stop = 0.0;
   }
}
